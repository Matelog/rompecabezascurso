// Representación de la grilla. Cada nro representa a una pieza.
// El 9 es la posición vacía
var grilla =  [[1,2,3], [4,5,6], [7,8,9]];
var posicionVacia = {
    fila:2,
    columna:2
};
    // Esta función va a chequear si el Rompecabezas est&aacute; en la posición ganadora

function chequearSiGano(){
    var grillaGanadora = [[1,2,3], [4,5,6], [7,8,9]];
    for(var fil = 0; fil<grilla.length;fil++){
        for(var col = 0;col<grilla[fil].length;col++){
            if(grilla[fil][col]==grillaGanadora[fil][col]){
                    console.log("¡va bien!");
                    console.log(grilla);
            }else{
                console.log("Error, está mal");
                console.log(grilla);
                return false;
                }
            }	
    }
    return true;
}

function mostrarCartelGanador(){	
       alert("¡¡Ganaste!!");
}

    // Intercambia posiciones grilla y en el DOM
function intercambiarPosiciones(fila1, columna1, fila2, columna2){
        ///// CAMBIO LOGICO (ACTUALIZAMOS GRILLA)
    var aux = grilla[fila2][columna2];
    grilla[fila2][columna2] = grilla[fila1][columna1];
    grilla[fila1][columna1] = aux;

        ///// CAMBIO INTERFAZ (ACTUALIZAMOS IMGs EN HTML)

        //console.log('pieza img'+grilla[fila1][columna1]);
        //console.log('pieza img'+grilla[fila2][columna2]);
        
        
        
    var meg1 = document.getElementById('img'+grilla[fila1][columna1]);
    var nod1 = meg1.cloneNode();
        //falta clonar e intercambiar.
    var meg2 = document.getElementById('img'+grilla[fila2][columna2]);
    nod2 = meg2.cloneNode();

    var father = meg1.parentNode;
    father.replaceChild(nod2, meg1);
    father.replaceChild(nod1, meg2);
}

    // Actualiza la posición de la pieza vacía
function actualizarPosicionVacia(nuevaFila,nuevaColumna){
    posicionVacia.fila = nuevaFila;
    posicionVacia.columna = nuevaColumna;
}


    // Para chequear si la posicón está dentro de la grilla.
function posicionValida(fila, columna){
    if(fila>=0 && fila<3 && columna>=0 && columna<3){
        return true;
    } else{
        return false;
    }
}


    // Movimiento de fichas, en este caso la que se mueve es la blanca intercambiando
    // su posición con otro elemento
function moverEnDireccion(direccion){
      var nuevaFilaPiezaVacia;
      var nuevaColumnaPiezaVacia;

      // Intercambia pieza blanca con la pieza que está arriba suyo
      if(direccion == 40){
        nuevaFilaPiezaVacia = posicionVacia.fila-1;
        nuevaColumnaPiezaVacia = posicionVacia.columna;
      }
      // Intercambia pieza blanca con la pieza que está abajo suyo
      else if (direccion == 38) {
        nuevaFilaPiezaVacia = posicionVacia.fila+1;
        nuevaColumnaPiezaVacia = posicionVacia.columna;
      }
      // Intercambia pieza blanca con la pieza que está a su izq
      else if (direccion == 39) {
        nuevaFilaPiezaVacia = posicionVacia.fila;
        nuevaColumnaPiezaVacia = posicionVacia.columna-1;    
      }
      // Intercambia pieza blanca con la pieza que está a su der
      else if (direccion == 37) {
        nuevaFilaPiezaVacia = posicionVacia.fila;
        nuevaColumnaPiezaVacia = posicionVacia.columna+1;
      }

      // Se chequea si la nueva posición es válida, si lo es, se intercambia 
      if (posicionValida(nuevaFilaPiezaVacia, nuevaColumnaPiezaVacia)){
        intercambiarPosiciones(posicionVacia.fila, posicionVacia.columna, nuevaFilaPiezaVacia, nuevaColumnaPiezaVacia);
        actualizarPosicionVacia(nuevaFilaPiezaVacia, nuevaColumnaPiezaVacia);
      }

    }



    // Extras, ya vienen dadas
function mezclarPiezas(veces){
  if(veces<=0){return;}
  var direcciones = [40, 38, 39, 37];
  var direccion = direcciones[Math.floor(Math.random()*direcciones.length)];
  moverEnDireccion(direccion);

  setTimeout(function(){
        mezclarPiezas(veces-1);
        mezclarPiezas;
      },500);
    }
function capturarTeclas(){
  document.body.onkeydown = (function(evento) {
    if(evento.which == 40 || evento.which == 38 || evento.which == 39 || evento.which == 37){
      moverEnDireccion(evento.which);
          var gano = chequearSiGano();
          if(gano){
            setTimeout(function(){
              mostrarCartelGanador();  
            },500);
          } 
          evento.preventDefault();
        }
      })
    }

function iniciar(){
  mezclarPiezas(60);
  capturarTeclas();
}
iniciar();
    
  
